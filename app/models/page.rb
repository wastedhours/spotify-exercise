class Page < ApplicationRecord
  belongs_to :product
  has_many :benefits
end
