class Product < ApplicationRecord
  has_many :pages

  has_one_attached :header_image
end
