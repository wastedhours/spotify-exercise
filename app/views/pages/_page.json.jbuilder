json.extract! page, :id, :lead, :subtitle, :cta_text, :sub_cta, :terms, :benefits_header, :product_name, :product_price, :product_frequency, :product_info, :product_features, :product_cta, :language, :product_id_id, :created_at, :updated_at
json.url page_url(page, format: :json)
