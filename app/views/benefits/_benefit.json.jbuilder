json.extract! benefit, :id, :pages_id, :image, :bold, :information, :language, :created_at, :updated_at
json.url benefit_url(benefit, format: :json)
