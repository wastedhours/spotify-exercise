json.extract! product, :id, :url, :header_color, :header_text_color, :header_image, :header_cta_colour, :header_cta_text_color, :country, :currency, :name, :created_at, :updated_at
json.url product_url(product, format: :json)
