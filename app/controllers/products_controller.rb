class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :require_admin, except: [:view, :index, :language]

  # GET /products
  # GET /products.json
  def index
    @products = Product.all
  end

  def language
    cookies[:language] = { value: params[:language], expires: 1.year.from_now, domain: :all, path: "/" }
    redirect_to cookies[:last_page]
    flash[:notice] = "Language settings updated!"    
  end

  def country
    cookies[:country] = { value: params[:country], expires: 1.year.from_now, domain: :all, path: "/" }
    redirect_to cookies[:last_page]
    flash[:notice] = "Country settings updated!"    
  end  

  def view
    @product = Product.where("country = ? AND url = ?", params[:country], params[:url]).last
    @language = cookies[:language] || "en" 
    @page = @product.pages.where("language = ?", @language).last
    @features = @page.product_features.split("//")

    cookies[:last_page] = { value: "/#{params[:country]}/#{params[:url]}", expires: 1.year.from_now, domain: :all, path: "/" }
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:url, :header_color, :header_text_color, :header_image, :header_cta_colour, :header_cta_text_color, :country, :currency, :name)
    end
end
