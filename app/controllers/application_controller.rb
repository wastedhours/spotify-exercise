class ApplicationController < ActionController::Base

  def require_admin
    redirect_to root_path unless current_user && current_user.email == "wastedhours@gmail.com"
  end  

end
