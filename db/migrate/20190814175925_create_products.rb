class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :url
      t.string :header_color
      t.string :header_text_color
      t.string :header_image
      t.string :header_cta_colour
      t.string :header_cta_text_color
      t.string :country
      t.string :currency
      t.string :name

      t.timestamps
    end
  end
end
