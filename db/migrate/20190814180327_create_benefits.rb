class CreateBenefits < ActiveRecord::Migration[6.0]
  def change
    create_table :benefits do |t|
      t.references :page, null: false, foreign_key: true
      t.string :image
      t.string :bold
      t.string :information
      t.string :language

      t.timestamps
    end
  end
end
