class CreatePages < ActiveRecord::Migration[6.0]
  def change
    create_table :pages do |t|
      t.string :lead
      t.string :subtitle
      t.string :cta_text
      t.string :sub_cta
      t.string :terms
      t.string :benefits_header
      t.string :product_name
      t.float :product_price
      t.string :product_frequency
      t.string :product_info
      t.text :product_features
      t.string :product_cta
      t.string :language
      t.references :product, null: false, foreign_key: true

      t.timestamps
    end
  end
end
