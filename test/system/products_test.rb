require "application_system_test_case"

class ProductsTest < ApplicationSystemTestCase
  setup do
    @product = products(:one)
  end

  test "visiting the index" do
    visit products_url
    assert_selector "h1", text: "Products"
  end

  test "creating a Product" do
    visit products_url
    click_on "New Product"

    fill_in "Country", with: @product.country
    fill_in "Currency", with: @product.currency
    fill_in "Header color", with: @product.header_color
    fill_in "Header cta colour", with: @product.header_cta_colour
    fill_in "Header cta text color", with: @product.header_cta_text_color
    fill_in "Header image", with: @product.header_image
    fill_in "Header text color", with: @product.header_text_color
    fill_in "Name", with: @product.name
    fill_in "Url", with: @product.url
    click_on "Create Product"

    assert_text "Product was successfully created"
    click_on "Back"
  end

  test "updating a Product" do
    visit products_url
    click_on "Edit", match: :first

    fill_in "Country", with: @product.country
    fill_in "Currency", with: @product.currency
    fill_in "Header color", with: @product.header_color
    fill_in "Header cta colour", with: @product.header_cta_colour
    fill_in "Header cta text color", with: @product.header_cta_text_color
    fill_in "Header image", with: @product.header_image
    fill_in "Header text color", with: @product.header_text_color
    fill_in "Name", with: @product.name
    fill_in "Url", with: @product.url
    click_on "Update Product"

    assert_text "Product was successfully updated"
    click_on "Back"
  end

  test "destroying a Product" do
    visit products_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Product was successfully destroyed"
  end
end
