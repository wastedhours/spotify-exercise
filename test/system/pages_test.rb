require "application_system_test_case"

class PagesTest < ApplicationSystemTestCase
  setup do
    @page = pages(:one)
  end

  test "visiting the index" do
    visit pages_url
    assert_selector "h1", text: "Pages"
  end

  test "creating a Page" do
    visit pages_url
    click_on "New Page"

    fill_in "Benefits header", with: @page.benefits_header
    fill_in "Cta text", with: @page.cta_text
    fill_in "Language", with: @page.language
    fill_in "Lead", with: @page.lead
    fill_in "Product cta", with: @page.product_cta
    fill_in "Product features", with: @page.product_features
    fill_in "Product frequency", with: @page.product_frequency
    fill_in "Product id", with: @page.product_id_id
    fill_in "Product info", with: @page.product_info
    fill_in "Product name", with: @page.product_name
    fill_in "Product price", with: @page.product_price
    fill_in "Sub cta", with: @page.sub_cta
    fill_in "Subtitle", with: @page.subtitle
    fill_in "Terms", with: @page.terms
    click_on "Create Page"

    assert_text "Page was successfully created"
    click_on "Back"
  end

  test "updating a Page" do
    visit pages_url
    click_on "Edit", match: :first

    fill_in "Benefits header", with: @page.benefits_header
    fill_in "Cta text", with: @page.cta_text
    fill_in "Language", with: @page.language
    fill_in "Lead", with: @page.lead
    fill_in "Product cta", with: @page.product_cta
    fill_in "Product features", with: @page.product_features
    fill_in "Product frequency", with: @page.product_frequency
    fill_in "Product id", with: @page.product_id_id
    fill_in "Product info", with: @page.product_info
    fill_in "Product name", with: @page.product_name
    fill_in "Product price", with: @page.product_price
    fill_in "Sub cta", with: @page.sub_cta
    fill_in "Subtitle", with: @page.subtitle
    fill_in "Terms", with: @page.terms
    click_on "Update Page"

    assert_text "Page was successfully updated"
    click_on "Back"
  end

  test "destroying a Page" do
    visit pages_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Page was successfully destroyed"
  end
end
