require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @page = pages(:one)
  end

  test "should get index" do
    get pages_url
    assert_response :success
  end

  test "should get new" do
    get new_page_url
    assert_response :success
  end

  test "should create page" do
    assert_difference('Page.count') do
      post pages_url, params: { page: { benefits_header: @page.benefits_header, cta_text: @page.cta_text, language: @page.language, lead: @page.lead, product_cta: @page.product_cta, product_features: @page.product_features, product_frequency: @page.product_frequency, product_id_id: @page.product_id_id, product_info: @page.product_info, product_name: @page.product_name, product_price: @page.product_price, sub_cta: @page.sub_cta, subtitle: @page.subtitle, terms: @page.terms } }
    end

    assert_redirected_to page_url(Page.last)
  end

  test "should show page" do
    get page_url(@page)
    assert_response :success
  end

  test "should get edit" do
    get edit_page_url(@page)
    assert_response :success
  end

  test "should update page" do
    patch page_url(@page), params: { page: { benefits_header: @page.benefits_header, cta_text: @page.cta_text, language: @page.language, lead: @page.lead, product_cta: @page.product_cta, product_features: @page.product_features, product_frequency: @page.product_frequency, product_id_id: @page.product_id_id, product_info: @page.product_info, product_name: @page.product_name, product_price: @page.product_price, sub_cta: @page.sub_cta, subtitle: @page.subtitle, terms: @page.terms } }
    assert_redirected_to page_url(@page)
  end

  test "should destroy page" do
    assert_difference('Page.count', -1) do
      delete page_url(@page)
    end

    assert_redirected_to pages_url
  end
end
