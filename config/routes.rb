Rails.application.routes.draw do

  resources :benefits
  resources :pages
  resources :products
  devise_for :users

  root to: "pages#home"

  get 'terms', to: 'pages#terms', as: 'terms'

  get 'languages', to: 'pages#languages', as: 'languages'
  get 'countries', to: 'pages#countries', as: 'countries'
  get 'language/:language', to: 'products#language', as: 'language'
  get 'country/:country', to: 'products#country', as: 'country'

  get ':country/:url', to: 'products#view', as: 'view_product'  

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
